﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookRegistration
{
    class Book
    {
        private string author;
        private string name;
        private int yearPublishing;
        private string publishingHouse;

        public Book(string author, string name, int yearPublishing, string publishingHouse)
        {
            this.author = author;
            this.name = name;
            this.yearPublishing = yearPublishing;
            this.publishingHouse = publishingHouse;
        }

        public override string ToString()
        {
            return $"Название книги: {name} Автор:{author} Год издания:{yearPublishing} Издательство:{publishingHouse}";
        }


        public int YearPublishing
        {
            get { return yearPublishing; }
        }

        public string Author
        {
            get { return author; }
        }

        public string PublishingHouse
        {
            get
            {
                return publishingHouse;
            }
        }

        public void Draw()
        {

        }

    }
}
