﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookRegistration
{
    class Program
    {
        static void Menu(List<Book> books)
        {
            Console.Clear();
            bool isEnd;
            int numMenu;

            do
            {
                Console.Clear();
                isEnd = false;

                do
                {
                    numMenu = InputInt("Выберите пункт меню:\n1)Добавить новую книгу\n2)Поиск книги\n3)Купить книгу\n0)Выход\n");

                } while (numMenu<0||numMenu>3);
                
                switch (numMenu)
                {
                    case 1:
                        AddNewBook(books);
                        break;
                    case 2:
                        SearchBook(books);
                        break;
                    case 3:
                        BuyBook(books);
                        break;
                    case 0:
                        isEnd = true;
                        break;
                }

            } while (!isEnd);
            
        }
        static void AddNewBook(List<Book> books)
        {
            Console.Clear();
            Console.WriteLine("Введите автора книги");
            string newAuthor = Console.ReadLine();

            Console.WriteLine("Введите название книги");
            string newName = Console.ReadLine();

            int newYearPub;

            do
            {
                newYearPub = InputInt("Введите год издания");

            } while (newYearPub<1);
            

            Console.WriteLine("Введите издательство");
            string newPublishHouse = Console.ReadLine();

            Book newBook = new Book(newAuthor, newName, newYearPub, newPublishHouse);

            books.Add(newBook);
        }

        static void SearchBook(List<Book> books)
        {
            if (books.Count == 0)
            {
                Console.WriteLine("Магазин пуст! Для продолжения нажмите <Enter>");
                Console.ReadKey();
            }
            else
            {
                Console.Clear();
                List<Book> searchList = new List<Book>();
                int variant;

                do
                {
                    variant = InputInt($"Выберите вариант поиска:\n1)По году выпуска\n2)По автору\n3)По издательству");
                } while (variant < 1 || variant > 3);


                switch (variant)
                {
                    case 1:
                        int minYear;

                        do
                        {
                            minYear = InputInt("Введите начальный диапазон поиска: ");
                        } while (minYear < 1);

                        int maxYear = InputInt("Введите конечный диапазон поиска: ");

                        for (int i = 0; i < books.Count; i++)
                        {
                            if (books[i].YearPublishing >= minYear && books[i].YearPublishing <= maxYear)
                            {
                                Console.WriteLine(books[i]);
                            }

                            Console.ReadKey();
                        }
                        break;

                    case 2:
                        Console.WriteLine("Введите фамилию автора");
                        string surname = Console.ReadLine();

                        searchList = books.FindAll(item => item.Author == surname).ToList();
                        PrintBookList(searchList);
                        Console.ReadKey();
                        break;

                    case 3:
                        Console.WriteLine("Введите издательство");
                        string publishingHouse = Console.ReadLine();

                        searchList = books.FindAll(item => item.PublishingHouse == publishingHouse).ToList();
                        PrintBookList(searchList);
                        Console.ReadKey();
                        break;
                }
            }

            
        }

        static void BuyBook(List<Book> books)
        {
            if (books.Count == 0)
            {
                Console.WriteLine("Магазин пуст! Для продолжения нажмите <Enter>");
                Console.ReadKey();
            }
            else
            {
                PrintBookList(books);
                int numID;
                do
                {
                    numID = InputInt("Выберите ID книги которую хотите купить: ");
                } while (numID < 0 || numID >= books.Count);

                CreateBuyer(books, numID);
                Console.ReadKey();
            }
            
            
        }

        static void CreateBuyer(List<Book> books, int numID)
        {
            Console.WriteLine("Введите имя покупателя:");
            string buyerName = Console.ReadLine();
            Console.WriteLine("Введите номер телефона:");
            string buyerTelephone = Console.ReadLine();

            Buyer buyer = new Buyer(buyerName, buyerTelephone);

            buyer.BuyBooks.Add(books[numID]);

            Console.WriteLine($"{buyerName} добро пожаловать в наш интернет-магазин. Вы купили: ");
            PrintBookList(buyer.BuyBooks);
        }

        static void PrintBookList(List<Book> books)
        {
            for (int i = 0; i < books.Count; i++)
            {
                Console.WriteLine($"ID:{i} " + books[i]);
            }
        }

        static int InputInt(string message)
        {
            bool inputResult;
            int numInt;
            do
            {
                Console.Write(message);
                inputResult = int.TryParse(Console.ReadLine(), out numInt);
            } while (!inputResult);

            return numInt;
        }



        static void Main(string[] args)
        {
            List<Book> books = new List<Book>();

            Menu(books);

            Console.ReadKey();
        }
    }
}
