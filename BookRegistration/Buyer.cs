﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookRegistration
{
    class Buyer
    {
        private string name;
        private string telephone;

        List<Book> buyBooks;

        public Buyer(string name, string telephone)
        {
            this.name = name;
            this.telephone = telephone;
            buyBooks=new List<Book>();
        }

        public List<Book> BuyBooks
        {
            get { return buyBooks; }
        }
    }
}
